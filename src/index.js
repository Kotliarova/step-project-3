import './index.html';
import './index.scss';

import { VisitForm } from "./modules/elementForm.js";
import { addVisitBtn, logInBtn, logOutBtn, footer, formFilter } from './modules/variables.js';
import { removeToken, toggleButtons, filterCards, handleFormFilter, logInBtnClick } from './modules/functions.js';
import {Request} from "./modules/request";

// logout event
logOutBtn.addEventListener('click', () => {
    toggleButtons();
    removeToken();
    footer.style.backgroundColor = "#230B66";
});

addVisitBtn.addEventListener('click', ev => {
    ev.preventDefault();
    const visitForm = new VisitForm();
    visitForm.createBaseVisitForm();
    visitForm.collectData();
})

logInBtn.addEventListener('click', logInBtnClick);
// listener for search form
formFilter.addEventListener('input', filterCards);
// listener for clear form inputs
formFilter.addEventListener('keydown', handleFormFilter);

// Request.deleteCard(152534)